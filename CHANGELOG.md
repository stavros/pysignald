# Changelog


## v0.1.1 (2023-05-01)

### Features

* Support mentions. [Sebastian Haas]

### Fixes

* Fix run_chat replies. [Sebastian Haas]


## v0.1.0 (2022-07-30)

### Features

* Add payments API. [Eran Rundstein]

### Fixes

* Upgrade dependencies to fix some Home Assistant conflicts. [Sebastian Haas]

* Check more locations for the socket file (fixes #10) [Sebastian Haas]

* Properly initialize `response_data` (fixes #9) [Stavros Korokithakis]

* Add captcha parameter to register method. [Robert Aistleitner]

* Support attachments. [sh]

* Fix some API breakage. [Stavros Korokithakis]


