VERSION 0.6
FROM python:3.9
WORKDIR /code

build:
    RUN pip install -U poetry pre-commit
    COPY pyproject.toml .
    RUN poetry config virtualenvs.create false
    RUN poetry install --no-interaction --no-root
    COPY .pre-commit-config.yaml .
    # Prevent a "no git repo" error when installing hooks.
    RUN git init
    # Install hooks to cache the dependencies.
    RUN pre-commit install --install-hooks
    SAVE IMAGE --push pysignald-build:latest

src:
    COPY . src
    SAVE ARTIFACT src /src

pre-commit:
    FROM +build
    COPY +src/src .
    RUN pre-commit run -a --hook-stage manual

test:
    FROM +build
    COPY +src/src .
    RUN pytest

test-all:
    BUILD +pre-commit
    BUILD +test
